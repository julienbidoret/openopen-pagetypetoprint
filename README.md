# PageTypeToPrint @ Open Open

Slides de la présentation de l’outil [PageTypeToPrint](https://github.com/esadpyrenees/PageTypeToPrint/) à l’occasion des journées Open Open à Cambrai en mai 2023.

## URLs

Les URLs intégrées dans les slides sont :

- https://accentgrave.net/
- https://ateliers.esad-pyrenees.fr/web/
- https://maisondeseditions.fr/fr
- https://accentgrave.net/machines/#inquarto
- https://reels.accentgrave.net/
- https://radicalweb.design/fr
- https://i.liketightpants.net/and/hybrid-publishing-back-to-the-future-publishing-theses-at-the-kabk
- https://kabk.github.io/go-theses-17-jordy-ringeling/
- https://kabk.github.io/govt-theses-16-elizaveta-pritychenko-believe-it-or-not/
- https://ateliers.esad-pyrenees.fr/web/pages/ressources/ctrl-alt-print/
- https://ateliers.esad-pyrenees.fr/web/archives/2022-2023/identites-numeriques/index.html
- https://ateliers.esad-pyrenees.fr/web/archives/2021-2022/web2print/aurore/index.html
- https://ateliers.esad-pyrenees.fr/web/archives/2019-2020/caution-hot-fluid/index.html
- https://workshops.esad-pyrenees.fr/2019/karaoprint/info/brief.html
- https://ateliers.esad-pyrenees.fr/web/archives/2022-2023/5DGM/Salom%C3%A9/
- https://ateliers.esad-pyrenees.fr/web/archives/2022-2023/5DGM/Salom%C3%A9/
- https://ateliers.esad-pyrenees.fr/web/archives/2022-2023/5DGM/Salom%C3%A9/print.php
- https://clumsyfemlab.philippinetalamona.com/
- https://ateliers.esad-pyrenees.fr/pagetypetoprint-booklet/?print
- https://paged.accentgrave.net/bricoles/
- https://paged.accentgrave.net/bricoles/?print
- https://paged.accentgrave.net/artnum/
- https://esadpyrenees.github.io/PageTypeToPrint/
- https://esadpyrenees.github.io/PageTypeToPrint/images/index.html
- https://esadpyrenees.github.io/PageTypeToPrint/microtypo/index.html
- https://constantvzw.org/wefts/cc4r.en.html
- https://ateliers.esad-pyrenees.fr/web/pages/exemples/

## Slides

Présentation basée sur un simple [script](https://github.com/esapyrenees/pechakucha/) “PechaKucha” développé pour les étudiant⋅es de l’ÉSAD Pyrénées.